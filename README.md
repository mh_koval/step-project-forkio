### Список использованных технологий
1. reset-css
2. jquery
3. google fonts
4. gulp
5. browsersync
6. gulp-sass
7. gulp-jsmin
8. gulp-clean-css
9. gulp-clean
10. gulp-concat
11. gulp-imagemin
12. gulp-autoprefixer
13. gulp-purgecss
14. gulp-rename

### Участники проекта

[Mykhayl Koval](https://gitlab.com/mh_koval), [Roman Butynskyj](https://gitlab.com/romanbutynskyj)

### Задачи разработчиков

Mykhayl Koval - шапка сайта, выпадающее меню, секция `People Are Talking About Fork`

Roman Butynskyj - секции `Revolutionary Editor`, `Here is what you get`, `Fork Subscription Pricing`.
